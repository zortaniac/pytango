---
stages:
  - check
  - build
  - image
  - test
  - release

default:
  interruptible: false

variables:
  CPP_TANGO_VERSION: "10.0.0dev0"
  TANGO_TEST_VERSION: "3.8"
  TWINE_USERNAME: __token__
  TWINE_PASSWORD: secret
  PYTHONUNBUFFERED: "1"

.macos:setup-micromamba:
  script:
    # Default shell is zsh on macOS but gitlab-runners are starting a bash shell
    - curl -L micro.mamba.pm/install.sh | bash
    - source /Users/gitlab/.bash_profile

.win:setup-micromamba:
  script:
    - |
      if ( !$CI_RUNNER_TAGS.Contains("desy") ) {
        Invoke-Webrequest -URI https://micro.mamba.pm/api/micromamba/win-64/latest -OutFile micromamba.tar.bz2
        7z x micromamba.tar.bz2 -aoa
        7z x micromamba.tar -aoa -ttar -r Library\bin\micromamba.exe
        MOVE -Force Library\bin\micromamba.exe micromamba.exe
        .\micromamba.exe shell init -s powershell -p "C:\\micromambaenv\\"
        . C:\Users\gitlab_runner\Documents\WindowsPowerShell\profile.ps1
      }

.matrix-wheel-linux:
  image: ${IMAGE_REGISTRY}:${MANYLINUX}_${ARCH}_v1.6.0.dev1
  tags:
    - linux
    - docker
    - $RUNNER_TAG
  variables:
    IMAGE_REGISTRY: registry.gitlab.com/tango-controls/docker/pytango-builder
    MANYLINUX: manylinux2014
    GLIBC_TAG: '2_17' # depends on manylinux
    PYTHON_TAG: 'cp${PYTHON_VER}-cp${PYTHON_VER}${ABI_SUFFIX}'
    WHEEL_REGEX: pytango*${PYTHON_TAG}*${ARCH}.whl
  parallel:
    matrix:
      - RUNNER_TAG: amd64
        ARCH: [x86_64, i686]
        PYTHON_VER: [39, 310, 311, 312]
      - RUNNER_TAG: aarch64
        ARCH: aarch64
        PYTHON_VER: [39, 310, 311, 312]

.matrix-wheel-macos:
  image: macos-12-xcode-14
  tags:
    - saas-macos-medium-m1
  parallel:
    matrix:
      - PLATFORM: ["osx-arm64", "osx-64"]
        PYTHON_VERSION: ["3.9", "3.10", "3.11", '3.12']
        BOOST_VERSION: "1.82.0"

.matrix-wheel-win:
  tags:
    - windows
    - desy
  parallel:
    matrix:
        - PYTHON_VERSION: ["3.9", "3.10", "3.11", '3.12']
          ARCH: ["Win32", "x64"]
          BOOST_VERSION: "1.83.0"

.rules-wheel:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_TAG'
    - if: $CI_JOB_NAME =~ /.*win:.*-wheel.*/ && $CPP_TANGO_VERSION =~ /.*dev.*/ && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual
      allow_failure: true
    - if: $CI_JOB_NAME =~ /.*macos:.*-wheel.*/ && $CPP_TANGO_VERSION =~ /.*dev.*/ && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual
      allow_failure: true
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual
      allow_failure: true

run-triage:
  stage: check
  image: ruby:3.2-slim
  tags:
    - linux
    - docker
    - amd64
  script:
    - gem install gitlab-triage
    - gitlab-triage --token $GITLAB_TRIAGE_API_TOKEN --source-id $CI_PROJECT_PATH
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"

run-pre-commit:
  stage: check
  image: registry.gitlab.com/tango-controls/docker/pre-commit
  tags:
    - linux
    - docker
    - amd64
  script:
    - pre-commit run --all-files

.build-wheel:
  stage: build
  artifacts:
    expire_in: 1 day
    paths:
      - dist/
  rules:
    - !reference [.rules-wheel, rules]

linux:build-wheel:
  extends: [.matrix-wheel-linux, .build-wheel]
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CMAKE_ARGS: "--preset=ci-Linux-quiet"
  script:
    # build wheel
    - export BOOST_PYTHON_SUFFIX=$(/opt/python/${PYTHON_TAG}/bin/python -c "import sys;print(f'{sys.version_info[0]}{sys.version_info[1]}')")
    - |
      if [[ "$ARCH" == "i686" && "$PYTHON_VER" == "312" ]]; then
        /opt/python/${PYTHON_TAG}/bin/python -m venv /src/venv
        source /src/venv/bin/activate
        python -m pip install --prefer-binary -Csetup-args=-Dallow-noblas=true numpy==1.26.1
        python -m pip install build "scikit-build-core[pyproject]>=0.6.0" oldest-supported-numpy ninja pybind11-stubgen
        python -m build --wheel --no-isolation
      else
        /opt/python/${PYTHON_TAG}/bin/python -m pip install build
        /opt/python/${PYTHON_TAG}/bin/python -m build --wheel
      fi
    # repair wheel
    - auditwheel repair dist/${WHEEL_REGEX}
    - rm -rf dist/${WHEEL_REGEX} # delete unrepaired wheel
    # copy wheel to dist
    - cp wheelhouse/${WHEEL_REGEX} ./dist # are we always sure of the name?

macos:build-wheel:
  extends: [.matrix-wheel-macos, .build-wheel]
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CMAKE_ARGS: "--preset=ci-macOS"
  before_script:
    - !reference [.macos:setup-micromamba, script]
    - |
      if [[ "$PLATFORM" == "osx-arm64" ]]; then
        export MACOSX_DEPLOYMENT_TARGET="11.0"
      else
        export MACOSX_DEPLOYMENT_TARGET="10.9"
      fi
    - >
      micromamba install -y -n base -c conda-forge
      -c conda-forge/label/cpptango_rc --platform $PLATFORM
      cpptango=${CPP_TANGO_VERSION} boost=${BOOST_VERSION} cppzmq cxx-compiler pkg-config
      python=${PYTHON_VERSION} pip build wheel delocate cmake ninja
    - micromamba activate base
  script:
    - python -m build --wheel
    # Include dependencies in wheel
    - delocate-wheel -v dist/*.whl

win:build-wheel:
  extends: [.matrix-wheel-win, .build-wheel]
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CMAKE_ARGS: "--preset=ci-Windows -A ${ARCH}"
    MSVC_VERSION: "v142"
    CPP_TANGO_PROJECT_ID:  "tango-controls%2FcppTango"
    TC_BOOST_CI_DOWNLOADS: "https://github.com/tango-controls/boost-ci/releases/download"
    CPP_TANGO_BUILD_TYPE: "shared_release"
  before_script:
    - $PythonTag = ${PYTHON_VERSION}.replace(".", "")
    - $PythonPath = "${PythonTag}"
    # DESY runner is shell-runner, so we have to reset env and use pre-installed python
    - |
      if ( $CI_RUNNER_TAGS.Contains("desy") ) {
        $env:PATH = Get-Content -Path c:\gitlab-runner\original_path.txt
        if ($ARCH -eq "Win32") { $PythonPath = "${PythonPath}-32" }
      } else {
        if ($ARCH -eq "Win32") { $Forcex86 = "--x86" }
        choco install -y python --version=${PYTHON_VERSION} $Forcex86
      }
    - $env:PATH = "C:\Python${PythonPath};C:\Python${PythonPath}\Scripts;" + $env:PATH
    - python -m pip install -U delvewheel build

    - $DEPARCH = if ($ARCH -eq "Win32") { "x86" } else { "x64" }
    - $BoostPython = "boost-python-${BOOST_VERSION}_${MSVC_VERSION}_${DEPARCH}_py${PythonTag}"
    - $LibTango = "libtango_${CPP_TANGO_VERSION}_${MSVC_VERSION}_${DEPARCH}_${CPP_TANGO_BUILD_TYPE}"

    - $DependenciesPath = "C:\dependencies\${PythonPath}"
    - mkdir -Force "${DependenciesPath}"

    # Get Boost Python
    - Invoke-WebRequest -URI "${TC_BOOST_CI_DOWNLOADS}/${BOOST_VERSION}/${BoostPython}.zip" -OutFile "${DependenciesPath}\boost.zip"
    - Expand-Archive -Path "${DependenciesPath}\boost.zip" -DestinationPath "${DependenciesPath}" -Force

    # Find the cppTango asset for the CPP_TANGO_VERSION we are targeting use gitlab's REST API
    - $TangoReleases = Invoke-Webrequest -UseBasicParsing "${CI_API_V4_URL}/projects/${CPP_TANGO_PROJECT_ID}/releases" | Select-Object -Expand Content | ConvertFrom-Json
    - $TangoAsset = ($TangoReleases | Where-Object { $_.tag_name -eq "$CPP_TANGO_VERSION" }).assets.links | Where-Object { $_.name -eq "${LibTango}.zip" }
    - $TangoAsset
    - $TangoUrl = $TangoAsset.url

    # Get cppTango
    - Invoke-WebRequest -URI $TangoUrl -OutFile libtango.zip
    - Expand-Archive -Path libtango.zip -DestinationPath "${DependenciesPath}" -Force

    # Setup environment
    - $env:BOOST_ROOT = "${DependenciesPath}\Boost"
    - $env:BOOST_PYTHON_SUFFIX = "${PythonTag}"
    - $env:TANGO_ROOT = "${DependenciesPath}\$LibTango"
    - $env:PYTHON_ROOT = "C:\Python${PythonPath}"
    - $env:PATH += ";$env:BOOST_ROOT\lib;$env:TANGO_ROOT\bin"
  script:
    - $WheelArch = if ($ARCH -eq "Win32") { "win32" } else { "win_amd64" }
    - $WheelGlob = "pytango*cp$PythonTag-cp$PythonTag*$WheelArch*"
    - python -m build --wheel
    - delvewheel repair $(Get-ChildItem -Path dist -Filter $WheelGlob | Select -Expand FullName)
    - Remove-Item dist\$WheelGlob
    - Move-Item wheelhouse\$WheelGlob dist\
  rules:
    - if: $CI_COMMIT_BRANCH =~ /.*windows.*/
    - !reference [.rules-wheel, rules]

linux:build-sdist:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
  image: python:3.9
  tags:
    - linux
    - docker
    - amd64
  before_script:
    - pip install twine build
  script:
    - python -m build --sdist
    - twine check dist/*
  artifacts:
    expire_in: 1 day
    paths:
      - dist/

build-docker-image:
  stage: image
  image: docker:latest
  tags:
    - linux
    - dind
    - amd64
  services:
    - docker:20.10.16-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_TLS_CERTDIR: ""
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - export COMMIT_TAG=$(echo $CI_COMMIT_TAG | sed --expression="s/v//g")
    - export LOWER_PROJECT_NAMESPACE=$(echo ${CI_PROJECT_NAMESPACE} | tr '[:upper:]' '[:lower:]')
    - cd .devcontainer # Minimize build context migrated to docker container
    - docker build -t $CI_REGISTRY/${LOWER_PROJECT_NAMESPACE}/$CI_PROJECT_NAME/pytango-dev:py${PYTHON_VERSION}-tango${CPP_TANGO_VERSION}-pytango${COMMIT_TAG} --build-arg PYTHON_VERSION --build-arg CPP_TANGO_VERSION -f Dockerfile .
    - docker push $CI_REGISTRY/${LOWER_PROJECT_NAMESPACE}/$CI_PROJECT_NAME/pytango-dev:py${PYTHON_VERSION}-tango${CPP_TANGO_VERSION}-pytango${COMMIT_TAG}
  parallel:
    matrix:
      - PYTHON_VERSION: ['3.9', '3.10', '3.11', '3.12']
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_TAG'
      when: manual
      allow_failure: true

linux:test-source:
  stage: test
  image: mambaorg/micromamba
  tags:
    - linux
    - docker
    - amd64
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    CMAKE_ARGS: "--preset=ci-Linux"
  # Avoid job to wait on wheels building
  needs: [linux:build-sdist]
  before_script:
    - ulimit -c unlimited
    - eval "$(/bin/micromamba shell hook -s bash)"
    # Install build dependencies
    - >
      micromamba install -y -n base -c conda-forge
      -c tango-controls/label/dev -c conda-forge/label/cpptango_rc
      -c conda-forge/label/tango-test_rc -c tango-controls/label/dev 
      cpptango-dbg=$CPP_TANGO_VERSION boost cppzmq cxx-compiler pkg-config
      python=$PYTHON_VERSION numpy git make
      packaging psutil pytest pytest-forked pytest-cov pystack gevent
      cmake clang-format clang-tools cppcheck ninja
    - micromamba activate base
    - | 
      if [[ $CPP_TANGO_VERSION == *"dev"* ]]; then
        echo "Building TangoTest against dev cpp_tango"
        pushd /tmp
        git clone --recurse-submodules https://gitlab.com/tango-controls/TangoTest.git
        cd TangoTest
        cmake --install-prefix="$CONDA_PREFIX" -B build
        make -C build install
        popd 
      else
        micromamba install -c conda-forge tango-test=${TANGO_TEST_VERSION}
      fi
    # Install pytango
    - python -m pip install -v -e ".[tests]"
  script:
    - pytest --cov --cov-branch --cov-report=term --cov-report=html --cov-report=xml
  after_script:
    # Core files configuration
    - python_executable=$(which python)
    - dline="================================================================================"
    - |
      pattern=$(cat /proc/sys/kernel/core_pattern)
      if [ "$pattern" == "core.%e.%p.%t" ]; then
        echo "Core pattern is correct: $pattern";
        for core_file in core.*; do
          if [ -f "$core_file" ]; then
            echo -e "$dline\nAnalyzing core file: $core_file\n$dline\n"
            pystack core $core_file $python_executable --native-all --locals
            echo -e "$dline\n"
          fi
        done
      else
        echo "Core pattern is incorrect(expected: core.%e.%p.%t): $pattern";
      fi
  parallel:
    matrix:
      - PYTHON_VERSION: ['3.9', '3.10', '3.11', '3.12']
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    paths:
      - htmlcov/
      - coverage.xml
      - report.xml
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

linux:test-main-cpptango:
  stage: test
  image: mambaorg/micromamba
  tags:
    - linux
    - docker
    - amd64
  # Avoid job to wait on wheels building
  needs: [linux:build-sdist]
  before_script:
    - eval "$(/bin/micromamba shell hook -s bash)"
    # Install build and test dependencies
    - >
      micromamba install -y -n base -c tango-controls/label/dev -c conda-forge
      cpptango boost cmake cppzmq cxx-compiler git make pkg-config ninja
      python numpy
      packaging psutil pytest pytest-forked gevent
    - micromamba activate base
    # build and install TangoTest
    - pushd /tmp
    - git clone --recurse-submodules https://gitlab.com/tango-controls/TangoTest.git
    - cd TangoTest
    - cmake --install-prefix="$CONDA_PREFIX" -B build
    - make -C build install
    - popd
    # Install pytango
    - python -m pip install -v $(ls dist/pytango*.tar.gz)[tests]
  script:
    - pytest
  artifacts:
    when: always
    paths:
      - report.xml
    reports:
      junit: report.xml
  rules:
    # Disable detached pipeline on MR
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: never
    - if: $CI_PIPELINE_SOURCE == "schedule"
      allow_failure: false
    - if: $CI_COMMIT_BRANCH || $CI_COMMIT_TAG
      when: manual
      allow_failure: true

linux:test-wheel:
  stage: test
  extends: .matrix-wheel-linux
  needs: [linux:build-wheel]
  before_script:
    - /opt/python/${PYTHON_TAG}/bin/python -m venv venv
    - source venv/bin/activate
    - pip install --upgrade pip
    - |
      if [[ "$ARCH" == "i686" ]]; then
        pip install --prefer-binary -Csetup-args=-Dallow-noblas=true numpy
      fi
    - pip install --prefer-binary $(find dist/${WHEEL_REGEX})[tests]
  script:
    - pytest
  rules:
    - if: $CI_COMMIT_TAG && $ARCH == "aarch64"
      allow_failure: true
    - !reference [.rules-wheel, rules]

macos:test-wheel:
  stage: test
  extends: .matrix-wheel-macos
  needs: [macos:build-wheel]
  before_script:
    - !reference [.macos:setup-micromamba, script]
    - ABI_TAG=$(echo "cp${PYTHON_VERSION}" | sed "s/\.//")
    - |
      if [[ "$PLATFORM" == "osx-arm64" ]]; then
        WHEEL_FILE=$(ls dist/pytango-*-${ABI_TAG}-macosx_*_arm64.whl)
      else
        WHEEL_FILE=$(ls dist/pytango-*-${ABI_TAG}-macosx_*_x86_64.whl)
      fi
    - micromamba install -y -n base -c conda-forge --platform $PLATFORM python=${PYTHON_VERSION} pip
    # Install TangoTest in a separate env to make sure the wheel isn't using
    # the tango libraries installed with it
    - micromamba create -y -n tango -c conda-forge --platform $PLATFORM tango-test=${TANGO_TEST_VERSION}
    - ln -s ~/micromamba/envs/tango/bin/TangoTest ~/micromamba/bin/TangoTest
    - micromamba activate base
    - echo "Testing $WHEEL_FILE"
    - python -m pip install ${WHEEL_FILE}[tests]
  script:
    - pytest
  rules:
    - !reference [.rules-wheel, rules]

win:test-wheel:
  stage: test
  extends: .matrix-wheel-win
  needs: [win:build-wheel]
  before_script:
    - !reference [.win:setup-micromamba, script]
    - micromamba install -y -n base -c conda-forge tango-test=$env:TANGO_TEST_VERSION
    - micromamba activate base
    - $PythonTag = ${PYTHON_VERSION}.replace(".", "")
    - $PythonPath = "${PythonTag}"
    - |
      if ( $CI_RUNNER_TAGS.Contains("desy") ) {
        $env:PATH = Get-Content -Path c:\gitlab-runner\original_path.txt
        if ($ARCH -eq "Win32") { $PythonPath = "${PythonPath}-32" }
      } else {
        if ($ARCH -eq "Win32") { $Forcex86 = "--x86" }
        choco install -y python --version=${PYTHON_VERSION} $Forcex86
      }
    - $env:PATH = "C:\Python${PythonPath};C:\Python${PythonPath}\Scripts;" + $env:PATH
    - $WheelArch = if ($ARCH -eq "Win32") { "win32" } else { "win_amd64" }
    - $WheelGlob = "pytango*cp$PythonTag-cp$PythonTag*$WheelArch*"
    - python -m pip install --force-reinstall "$(Get-ChildItem -Path dist -Filter $WheelGlob | Select -Expand FullName)[tests]"
  script:
    - $null > pytest_empty_config.txt
    - pytest --collect-only -q -k "not test_client.py and not gevent and not test_event"
    - .\tests\run_tests_win.bat
  after_script:
    - |
      if ( $CI_RUNNER_TAGS.Contains("desy") ) {
        $PythonPath = ${PYTHON_VERSION}.replace(".", "")
        if ($ARCH -eq "Win32") { $PythonPath = "${PythonPath}-32" }
        $env:PATH = "C:\Python${PythonPath};C:\Python${PythonPath}\Scripts;" + $env:PATH
        python -m pip uninstall -y pytango
      }
  rules:
    - if: $CI_COMMIT_BRANCH =~ /.*windows.*/
    - !reference [.rules-wheel, rules]

test-docs:
  # official build is on https://readthedocs.org/projects/pytango/ but we test doc generation
  stage: test
  image: sphinxdoc/sphinx:latest
  tags:
    - linux
    - docker
    - amd64
  # Avoid job to wait on wheels building
  needs: [linux:build-sdist]
  script:
    - python -m pip install numpy gevent psutil sphinx_rtd_theme
    - python -m sphinx doc build/sphinx
    - echo "Documentation can be found at https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/build/sphinx/index.html"
  artifacts:
    expire_in: 1 day
    paths:
      - build/sphinx
  environment:
    name: Docs-dev
    url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/build/sphinx/index.html"

release-pypi-package:
  stage: release
  image: python:3.9
  tags:
    - linux
    - docker
    - amd64
  before_script:
    - pip install twine
  script:
    - twine upload dist/*
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_COMMIT_TAG'
      when: manual

include:
  - project: 'tango-controls/gitlab-ci-templates'
    file: 'ArchiveWithSubmodules.gitlab-ci.yml'
